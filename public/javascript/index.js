rock_btn = document.querySelector('#rock_btn');
paper_btn = document.querySelector('#paper_btn');
scissors_btn = document.querySelector('#scissors_btn');
reset_btn = document.querySelector('#reset_btn');
player_score_elm = document.querySelector('#player_score');
computer_score_elm = document.querySelector('#computer_score');
per_round_results_elm = document.querySelector('#per_round_results');
final_results_elm = document.querySelector('#final_results');

let numRoundsPlayed = 0;
let winScore = 5;

function getComputerChoice(){
    let randNum = Math.floor(Math.random()*3);
    // randNum is either 0, 1, or 2

    if (randNum === 0){
        return "Rock";
    }else if(randNum === 1){
        return "Paper";
    }else{  // randNum === 2
        return "Scissors";
    }
}

/**
 * playerWon taker playerSelection and computerSelection and returns
 * True if player has won,
 * False if player has lost,
 * null if playerSelection is an invalid value
 */
function playerWon(playerSelection, computerSelection){
    switch (playerSelection.toLowerCase()){
        case "rock":
            return computerSelection.toLowerCase() === "scissors";
        case "paper":
            return computerSelection.toLowerCase() === "rock";
        case "scissors":
            return computerSelection.toLowerCase() === "paper";
        default:
            return null;
    }
}

/**
 * playRound taker playerSelection and computerSelection and returns
 * 1 if player has won,
 * 0 if player has lost,
 * -1 if it is a draw
 */
function playRound(playerSelection, computerSelection){
    if (playerSelection.toLowerCase() === computerSelection.toLowerCase()){
        return 0;
    }

    let playerWonBool = playerWon(playerSelection, computerSelection);
    if (playerWonBool === null){
        throw "Invalid playerSelection";
    }

    if(playerWonBool){
        return 1;
    }else{
        return -1;
    }
}

function handlePlayerInput(event) {
    computerSelection = getComputerChoice();

    round_result = playRound(event.target.innerText, computerSelection);

    player_score = parseInt(player_score_elm.innerText);
    computer_score = parseInt(computer_score_elm.innerText);
    if (player_score == winScore || computer_score == winScore){
        resetGameState();
        player_score = 0;
        computer_score = 0;
    }

    if (round_result == 1){  // player won
        // Display round result
        round_result_elm = document.createElement('p');
        round_result_elm.innerText = `Round ${numRoundsPlayed}: `;
        round_result_elm.innerText += `${event.target.innerText} beats ${computerSelection}.`;
        round_result_elm.innerText += `Player Won.`;
        round_result_elm.style.color = 'green';
        per_round_results_elm.appendChild(round_result_elm);

        // Update scores
        player_score++;
        player_score_elm.innerText = player_score;

        // Update num rounds played
        numRoundsPlayed++;
    }else if(round_result == -1){  // player lost
        // Display round result
        round_result_elm = document.createElement('p');
        round_result_elm.innerText = `Round ${numRoundsPlayed}: `;
        round_result_elm.innerText += `${computerSelection} beats ${event.target.innerText}.`;
        round_result_elm.innerText += `Computer Won.`;
        round_result_elm.style.color = 'red';
        per_round_results_elm.appendChild(round_result_elm);

        // Update scores
        computer_score++;
        computer_score_elm.innerText = computer_score;

        // Update num rounds played
        numRoundsPlayed++;
    }else{  // Round Draw
        // Display round result
        round_result_elm = document.createElement('p');
        round_result_elm.innerText = `Round ${numRoundsPlayed}: `;
        round_result_elm.innerText += `${event.target.innerText} vs ${computerSelection}.`;
        round_result_elm.innerText += `Draw.`;
        per_round_results_elm.appendChild(round_result_elm);

        // Update scores: no update

        // Update num rounds played
        numRoundsPlayed++;
    }

    if (player_score == winScore){
        final_result_para = document.createElement('h2');
        final_result_para.innerText = 'Final result: Player Won.';
        final_result_para.style.color = 'green';
        final_result_para.style.fontWeight = 'bold';
        final_result_para.style.textDecoration = 'underline';
        final_results_elm.appendChild(final_result_para);
        alert('Player Won.');
    }else if (computer_score == winScore){
        final_result_para = document.createElement('h2');
        final_result_para.innerText = 'Final result: Computer Won.';
        final_result_para.style.color = 'red';
        final_result_para.style.fontWeight = 'bold';
        final_result_para.style.textDecoration = 'underline';
        final_results_elm.appendChild(final_result_para);
        alert('Computer Won.');
    }else{
        // Nobody won yet
    }
}

function resetGameState() {
    numRoundsPlayed = 0;
    player_score_elm.innerText = 0;
    computer_score_elm.innerText = 0;
    while (per_round_results_elm.firstChild){
        per_round_results_elm.removeChild(per_round_results_elm.lastChild);
    }
    final_results_elm.innerText = '';
}

rock_btn.addEventListener('click', handlePlayerInput);
paper_btn.addEventListener('click', handlePlayerInput);
scissors_btn.addEventListener('click', handlePlayerInput);
reset_btn.addEventListener('click', resetGameState);
